Processing Environmental Rasters for MaPP Planning Units
====
Environmental layers were resampled to the extent and origin of the planning units

## Planning Units
* 1 km planning units from the MaPP process
The following layers were initially assembled for the PMECS work (Rubidge et al. 2016/035 CSAS Research Document)

## Nutrients  
 * 3 layers: Dissolved Oxygen, Nitrate, pH, Phosphate, and Silicate
 * Source: BioOracle

1. Data Downloaded from Bio-ORACLE (http://www.oracle.ugent.be/)

 Bio-ORACLE
 Bio-ORACLE is a set of GIS rasters providing marine environmental information for global-scale applications. It offers an array of geophysical, biotic and climate data at a spatial resolution 5 arcmin (9.2 km) in the ESRI ascii format.
 The database is documented in an article that is currently under review: Tyberghein L., Verbruggen H., Pauly K., Troupin C., Mineur F. & De Clerck O. Bio-ORACLE: a global environmental dataset for marine species distribution modeling.

 Data sources:
 * Ocean Color Web: http://oceancolor.gsfc.nasa.gov/ (sea surface temperature, photosynthetically available radiation, Chlorophyll A concentration, Diffuse Attenuation, Calcite concentration)
 *  World Ocean Database 2009: http://www.nodc.noaa.gov/ (Salinity, pH, Dissolved Oxygen, Silicate, Nitrate, Phosphate)
 * Nasa Earth Observation (NEO): http://neo.sci.gsfc.nasa.gov/Search.html (Cloud cover)

#### Processing steps for Nutrients
 1. Open .asc file in Arcmap

 2. Using ArcMap, Convert these global files from WGS 1984 and crop to a BC extent
  * Set data frame spatial referencfe to NAD 1983 BC Albers
  * Create a drawn rectangle with the exact extent of shapefule Chlorophyll_clipped_raster_extent.gdb
  * Select the rectangle
  * Export raster (right click - export data) with the following parameters

      > Cell size 4085.45*4085.45

      > Extent - Selected graphics (clipping)

      > Spatial reference - dataframe (current)

      > Format - TIFF

 3. Took the derivative files and resampled them to a common 4km grid cells matching the species data (process_Rasters_4km_2.R)

 >(res 4000*4000, extent 404060,1312060,0,1244000 (xmin, xmax, ymin, ymax))

 > `nutrfiles<-list.files(getwd(), pattern="(.*)clip.tif$", recursive=T)
 nutrrasters<-stack(nutrfiles)`


 * Then these resampled layers can be combined with the resampled SST, depth, water movement layers, adversity etc to go right into random forest

## Chlorophyll-A
* 2 layers: Bloom Frequency and Chl-A Mean
* Source: layers put together by Jessica Nephin (http://gitlab.ssc.etg.gc.ca/jnephin/Chlorophyll_a), using the version without the straylight filter


## Sea Surface Temperature
* 5 layers: Summer, fall, winter, spring, and mission composite ("overall")
* Source: OceanColor

#### Processing steps for Chlorophyll A data from NASA

1. Downloaded global 4km, level 3, 11um daytime Sea Surface Temperature AquaMODIS, .hdf files from NASA OceanColor (http://oceancolor.gsfc.nasa.gov/cgi/l3)
	-winter 2002-2013; spring, summer, and fall 2002-2014) and mission-long (“overall”; Jul 2002-Jan 2015) composites were downloaded and used as-is
 	-total of 5 SST input files.

2. Using free software SeaDAS (http://seadas.gsfc.nasa.gov/) to view metadata (check for scaling equations (none) and resolution)
-Using SeaDAS, preproject .hdf file to GeoTiff in WGS 1984

3. Using ArcMap, Convert these global files from WGS 1984 and crop to a BC extent

 * Set data frame spatial referencfe to NAD 1983 BC Albers

 * Create a drawn rectangle with the exact extent of shapefule Chlorophyll_clipped_raster_extent.gdb
 * Select the rectangle
 * Export raster (right click - export data) with the following parameters
    >Cell size 4085.45*4085.45

     >Extent - Selected graphics (clipping)

     >Spatial reference - dataframe (current)

     >Format - TIFF

4. Took these cropped files and resampled them to a common 4km grid cells matching the species data (process_Rasters_4km_2.R)
      > (res 4000*4000, extent 404060,1312060,0,1244000 (xmin, xmax, ymin, ymax))

      >  `chlorfiles<-list.files(getwd(), pattern = "ChlA(*.)",recursive=T) gridraster<-raster(resolution=c(4000),ymn=0,xmn=404060,xmx=1312060,ymx=1244000, vals=c(1:length(grid)), crs=crs(grid))
      chlormod<-resample(chlorrasters, gridraster, method="ngb")`

* Then these resampled layers can be combined with the resampled ChlA, depth, water chemistry layers, adversity etc to go right into random forest

## Depth and derivatives
* 4 layers: Depth, slope, aspect, and rugosity
* Source: depth layer from BCMCA. Slope, aspect, and rugostiy (surface to planar) from BTM tool

## Foreman layers
* 22 layers
* Source: model output from Mike Foreman's ocean circulation model, passed on by Aaron Eger.

Environmental Data from Mike Foreman's (IOS) Ocean Circulation Model
Citation for model:
	Foreman, M.G. G., Crawford, W.R., Cherniawsky, J.Y., Galbraith, J. 2008.
		Dynamic ocean topography for the northeast Pacific and its continental margins.
		Geophysical Research Letters, 35(22).


The original dataset was obtained by J. Finney (PBS) in 2012 for use in species distribution models (Finney, J.L. 2010. Overlap of predicted cold-water coral habitat and bottom-contact fisheries in British Columbia. DFO Can. Sci. Advis. Sec. Res. Doc. 2010/067. vi + 26 p).
The data was passed along to A. Eger (PBS, now at UVic) who forwared it to E. Rubidge and K. Gale (IOS) in 2014 for use in predictive modelling.

The model produces data at nodes with spacing varying from 100 m nearshore to > 50 km offshore, from surface to bottom.
This version of the data is the extracted bottom values only.

Description of layers:
Temperature and salinity: Data from CTD, bottle, expendable bathy-themograph, and Argo data was collated and interpolated for each node. Therefore the temperature and salinity values represent "real data", not a model output.
The exact data range that these values represent is not totally clear but is likely "historical" (~1940s) to 2010. Average values for Spring, summer, winter, and fall are presented, as well as the max, min, range for the seasonal values.
Summer=July through September, Fall= October through December, Winter=January through March, Spring=April through June.

#### Processing steps by K. Gale: 

1. Data for seasonal and min/max/range values exists in csv format with values at nodes

2. Convert csv into individual shapefiles for each variable (.shp format)

3. Carry out "spline with barriers" routine in ArcMap to create a raster (.tif format) of values at 500 m resolution

## Gregr files
* 3 layers: Adversity, Disturbance, and grain size
* Source: A habitat template for BC, Gregr et al.  
